#include <point.h>
#include <stdio.h>

int main(){
	Point p1,p2;
	float distancia;
	printf("Ingrese coordenadas\nPunto 1");
	printf("\nx1:");
	scanf("%f",&p1.x);
	printf("\ny1:");
	scanf("%f",&p1.y);
	printf("\nz1:");
	scanf("%f",&p1.z);
	printf("\n\nPunto 2");
	printf("\nx2:");
	scanf("%f",&p2.x);
	printf("\ny2:");
	scanf("%f",&p2.y);
	printf("\nz2:");
	scanf("%f",&p2.z);
	distancia = calcularDistancia(p1,p2);
	printf("\nDistancia euclidiana entre punto 1 y punto 2 es: %.2f",distancia);
}
